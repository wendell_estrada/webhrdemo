Ext.define('WebHr.model.Department', {
    extend: 'Ext.data.Model',
    idProperty: 'Id',
    identifier: {
        type: 'sequential'
    },
    fields: [
        {
            name: 'Id',
            type: 'int'
        },
        {
            name: 'Name',
            type: 'string'
        }
    ]
})