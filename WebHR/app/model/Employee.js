Ext.define('WebHr.model.Employee', {
    extend: 'Ext.data.Model',
    idProperty: 'Id',
    identifier: {
        type: 'sequential'
    },
    fields: [
        { name: 'Id', type: 'int' },
        { name: 'FirstName', type: 'string', flex: 1 },
        { name: 'LastName', type: 'string', flex: 1 },
        { name: 'MonthlyRate', type: 'number' },
        { name: 'DepartmentId' },
        { name: 'FullName', type: 'string' }
    ]
});
