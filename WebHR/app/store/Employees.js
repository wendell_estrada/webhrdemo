Ext.define('WebHr.store.Employees', {
    extend: 'Ext.data.Store',

    requires: [
        "WebHr.model.Employee"
    ],

    model: "WebHr.model.Employee",
    autoLoad: true,
    autoSync: false,
    batchActions: true,
    proxy: {
        type: 'rest',
        url: '/api/employees',
        api: {
            create: '/api/employees',
            read: '/api/employees',
            update: '/api/employees',
            destroy: '/api/employees'
        },
        reader: {
            type: 'json'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        },
        sorters: [
            {
                property: 'LastName',
                direction: 'ASC'
            }
        ]
    },

    isDirty: function () {
        var me = this, dirty = false;

        me.each(function (r) {
            dirty = dirty || r.dirty || r.phantom
        });

        dirty = dirty || me.getRemovedRecords().length;
        return !!dirty;
    }
});
