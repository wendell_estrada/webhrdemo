Ext.define('WebHr.store.DepartmentEmployees', {
    extend: 'Ext.data.Store',

    requires: [
        "WebHr.model.Employee"
    ],

    model: "WebHr.model.Employee",
    autoLoad: false,
    autoSync: false,
    batchActions: true,
    proxy: {
        type: 'rest',
        url: '/api/departmentemployees',
        reader: {
            type: 'json'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});
