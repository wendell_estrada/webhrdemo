Ext.define('WebHr.store.Departments', {
    extend: 'Ext.data.Store',

    requires: [
        "WebHr.model.Department"
    ],

    model: "WebHr.model.Department",
    autoLoad: true,
    autoSync: false,
    batchActions: true,
    proxy: {
        type: 'rest',
        url: '/api/departments',
        api: {
            create: '/api/departments',
            read: '/api/departments',
            update: '/api/departments',
            destroy: '/api/departments'
        },
        reader: {
            type: 'json'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});
