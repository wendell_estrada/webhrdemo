/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('WebHr.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'WebHr.view.main.MainController',
        'WebHr.view.main.MainModel',
        'WebHr.view.employee.EmployeeList',
        'WebHr.view.department.DepartmentList',
        'WebHr.view.department.DepartmentForm'
    ],

    xtype: 'app-main',

    controller: 'main',
    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'border'
    },

    items: [
        {
            region: 'center',
            layout: 'fit',
            xtype: 'tabpanel',
            tabBar: {
                // turn off borders for classic theme.  neptune and crisp don't need this
                // because they are borderless by default
                border: false
            },
            tabRotation: 0,
            tabPosition: 'left',
            items: [
                {
                    title: 'Employees',
                    xtype: 'employeelist'
                },
                {
                    title: 'Departments',
                    xtype: 'departmentemployees'
                }
            ]
        }
    ]
});
