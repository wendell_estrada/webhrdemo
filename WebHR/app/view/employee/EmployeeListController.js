Ext.define('WebHr.view.employee.EmployeeListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.employee-employeelist',

    onSelectionChange: function (selModel, selections) {
        var grid = this.getView();
        grid.down('#delete').setDisabled(selections.length === 0);

        var viewModel = this.getViewModel();
        var record = selections[0];
        viewModel.setData({ rec: record });
    }
});
