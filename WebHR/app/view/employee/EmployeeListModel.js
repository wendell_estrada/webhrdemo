Ext.define('WebHr.view.employee.EmployeeListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.employee-employeelist',

    data: {
        name: 'Employee Master List',
        rec: null
    },

    formulas: {
        fullname: function (get) {
            return get('rec.FirstName') + ' ' + get('rec.LastName');
        },
        rate: function (get) {
            return Ext.util.Format.number(get('rec.MonthlyRate'), '0,000.00');
        },
        dailyRate: function (get) {
            return Ext.util.Format.number(get('rec.MonthlyRate') * 12 / 214, '0,000.00')
        }
    }
});