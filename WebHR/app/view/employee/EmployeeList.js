// * for look up */
var departmentStore = Ext.create('WebHr.store.Departments', {autoLoad: true});
var employeestore = Ext.create('WebHr.store.Employees');

var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    listeners: {
        cancelEdit: function (rowEditing, context) {
            if (context.record.phantom) {
                employeestore.remove(context.record);
                employeestore.reload();
            }
        }
    }
});

Ext.define("WebHr.view.employee.EmployeeList", {
    extend: "Ext.grid.Panel",
    itemId: 'grid',
    alias: "widget.employeelist",

    requires: [
        "WebHr.view.employee.EmployeeListController",
        "WebHr.view.employee.EmployeeListModel",
        "WebHr.store.Employees"
    ],

    controller: "employee-employeelist",

    viewModel: {
        type: "employee-employeelist"
    },
    clicksToEdit: 1,
    plugins: [ rowEditing ],
    forceFit: true,

    listeners: {
        selectionchange: 'onSelectionChange'
    },

    initComponent: function () {
        this.callParent(arguments);
    },
    dockedItems: [
        {
            dock: 'bottom',
            xtype: 'panel',
            height: 100,
            bodyPadding: 10,
            bind: { html: '<font style="color:green"><b>{fullname}</b></font> has a monthly salary of <b>{rate} PhP</b> or a daily rate of <b>{dailyRate} PhP<b>.' }
        },
        {
            dock: 'top',
            xtype: 'toolbar',
            items: [
                {
                    text: 'Add',
                    handler: function () {
                        employeestore.insert(0, new WebHr.model.Employee());
                        rowEditing.startEdit(0, 0);
                    }
                },
                {
                    itemId: 'delete',
                    text: 'Delete',
                    disabled: true,
                    handler: function () {
                        var grid = this.up("#grid");
                        var selection = grid.getView().getSelectionModel().getSelection()[0];
                        if (selection) {
                            Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete this record?', function (button) {
                                if (button == 'yes') {
                                    employeestore.remove(selection);
                                    employeestore.sync();
                                }
                            });
                        }
                    }
                },
                '-',
                {
                    text: 'Save',
                    handler: function () {
                        employeestore.sync();
                        Ext.MessageBox.alert("Save", "Record saved.");
                    }
                },
                {
                    text: 'Refresh',
                    handler: function () {
                        employeestore.reload();
                    }
                }
            ]
        }
    ],
    store: employeestore,
    viewConfig: {
        deferEmptyText: false,
        emptyText: 'No data'
    },
    columns: [
        {
            text: 'Id',
            dataIndex: 'Id',
            hidden: true
        },
        {
            text: 'Name',
            dataIndex: 'FullName',
            renderer: function(value) {
                return "<b>" + value + "</b>";
            }
        },
        {
            text: 'First Name',
            dataIndex: 'FirstName',
            field: {
                xtype: 'textfield',
                validator: function (value) {
                    if (value == "" || value == null)
                        return "First name must not be blank.";
                    return true;
                }
            }
        },
        {
            text: 'Last Name',
            dataIndex: 'LastName',
            field: {
                xtype: 'textfield',
                validator: function (value) {
                    if (value == "" || value == null)
                        return "Last name must not be blank.";
                    return true;
                }
            }
        },
        {
            text: 'Monthly Rate',
            dataIndex: 'MonthlyRate',
            xtype: 'numbercolumn',
            field: {
                xtype: 'numberfield',
                validator: function (value) {
                    if (value <= 0)
                        return "Monthly rate must be greater than zero.";
                    return true;
                }
            }
        },
        {
            text: 'Department',
            dataIndex: 'DepartmentId',
            typeAhead: true,
            triggerAction: 'all',
            editor: {
                xtype: 'combobox',
                queryMode: 'remote',
                store: departmentStore,
                displayField: 'Name',
                valueField: 'Id'
            },
            renderer: function (id) {
                var recordIndex = departmentStore.find('Id', id);

                if (recordIndex === -1) {
                    return 'Unknown value: ' + id;
                }

                return departmentStore.getAt(recordIndex).get('Name');
            }
        }
    ]
});
