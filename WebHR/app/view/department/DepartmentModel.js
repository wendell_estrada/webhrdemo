var store = Ext.create('WebHr.store.Departments');
var empstore = Ext.create('WebHr.store.DepartmentEmployees');

Ext.define('WebHr.view.department.DepartmentModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.departments',

    data: {
        name: 'Departments',
        rec: null,
        store: store,
        empstore: empstore,
        editMode: ''
    },

    hasRecord: function() {
        this.set('rec', record);
        bind: '{rec}';
        return data.rec != null;
    }
});
