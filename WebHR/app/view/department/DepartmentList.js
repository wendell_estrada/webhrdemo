Ext.define('WebHr.view.department.DepartmentList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.departmentlist',
    bind: { store: '{store}' },
    reference: 'departmentGrid',
    listeners: {
        selectionchange: 'onSelectionChange',
        itemdblclick: 'onOpenDepartment'
    },

    viewModel: {
        type: 'departments'
    },

    initComponent: function() {
      this.callParent(arguments);
    },

    dockedItems: [
        {
            xtype: 'toolbar',
            hidden: false,
            dock: 'top',
            items: [
                {
                    text: 'Open',
                    hidden: false,
                    handler: 'onOpenDepartment'
                },
                {
                    text: 'Add',
                    handler: 'onAddDepartment'
                },
                {
                    text: 'Delete',
                    handler: 'onDelete'
                },
                {
                    text: 'Refresh',
                    reference: 'refreshDepartment',
                    handler: 'onRefreshDepartment'
                }
            ]
        }
    ],
    columns: [
        {
            text: "Id",
            dataIndex: 'Id',
            hidden: true
        },
        {
            text: 'Code',
            dataIndex: 'Code',
            bind: { text: '{rec.Code}' },
            flex: 1
        },
        {
            text: 'Name',
            dataIndex: 'Name',
            bind: { text: '{rec.Name}' },
            flex: 3
        }
    ]
});

Ext.define('WebHr.view.department.EmployeeDetailList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.employeedetaillist',
    bind: { store: '{empstore}' },
    viewConfig: {
        deferEmptyText: false,
        emptyText: 'There are no employees in this department.'
    },
    columns: [
        {
            text: "Id",
            dataIndex: 'Id',
            hidden: true
        },
        {
            text: 'Name',
            dataIndex: 'FullName',
            flex: 1
        },
        {
            text: 'First Name',
            dataIndex: 'FirstName',
            flex: 1,
            hidden: true
        },
        {
            text: 'Last Name',
            dataIndex: 'LastName',
            flex: 1,
            hidden: true
        },
        {
            text: 'Monthly Rate',
            dataIndex: 'MonthlyRate',
            xtype: 'numbercolumn',
            flex: 1
        }
    ]
});

Ext.define('WebHr.view.department.DepartmentEmployees', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.departmentemployees',
    requires: [
        "WebHr.view.department.DepartmentModel",
        "WebHr.view.department.DepartmentListViewController",
        "WebHr.store.DepartmentEmployees",
        "WebHr.view.department.EmployeeRateChart"
    ],
    controller: 'department-controller',

    viewModel: {
        type: 'departments'
    },

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'departmentlist',
            title: 'Departments',
            flex: 1
        },
        {
            split: true,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'employeedetaillist',
                    title: 'Employees',
                    bind: { title: 'Employees under <b>{rec.Name}</b>' },
                    flex: 1
                },
                {
                    xtype: 'employeechart',
                    flex: 2,
                    split: true
                }
            ],
            flex: 1
        },
        {
            xtype: 'window',
            itemId: 'depWindow',
            modal: true,
            layout: 'fit',
            closeAction: 'hide',
            bind: {
                title: '{rec.Name}'
            },
            width: 380,
            items: [
                {
                    xtype: 'departmentform'
                }
            ]
        }
    ]
});