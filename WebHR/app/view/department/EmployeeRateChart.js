Ext.define('WebHr.view.department.EmployeeRateChart', {
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.employeechart',
    width: 500,
    height: 500,
    flipXY: true,

    title: 'Employee Monthly Income',
    bind: {
        store: '{empstore}',
        title: 'Employee Monthly Income in <b>{rec.Name}</b>'
    },

    axes: [
        {
            type: 'numeric',
            position: 'bottom',
            grid: true,
            minimum: 0
        },
        {
            type: 'category',
            position: 'left'
        }
    ],

    series: [{
        type: 'bar',
        xField: ['FullName'],
        yField: ['MonthlyRate'],
        label: {
            field: 'MonthlyRate',
            display: 'insideEnd',
            renderer: function (value) {
                return Ext.util.Format.number(value, "0,000.00");
            }
        },
        axis: 'bottom',
        subStyle: {
            fill: ["#66b3ff"]
        }
    }]
});