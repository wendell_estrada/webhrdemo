Ext.define('WebHr.view.department.DepartmentListViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.department-controller',

    onSelectionChange: function (selModel, selections) {
        var grid = this.getView();

        var viewModel = this.getViewModel();
        var record = selections[0];
        if (record != null) {
            viewModel.setData({ rec: record });
            empstore.load({ id: record.get("Id") });
        }
    },

    onRefreshDepartment: function() {
        var viewModel = this.getViewModel();
        var store = viewModel.getData().store;
        var empStore = viewModel.getData().empstore;
        store.load();
        if (viewModel.getData().rec != null) {
            var id = viewModel.getData().rec.get("Id");
            if (id != null)
                empStore.load({ id: id });
        }
    },

    onOpenDepartment: function() {
        var viewModel = this.getViewModel();
        viewModel.setData({ editMode: 'add' });
        if (viewModel.getData().rec != null) {
            var id = viewModel.getData().rec.get("Id");
            if (id != null) {
                this.getView().floatingItems.get('depWindow').show();
            }
        }
    },

    onSave: function(e) {
        var viewModel = this.getViewModel();
        if (viewModel.getData().rec != null) {
            var store = viewModel.getData().store;
            store.sync();
            e.up('window').close();
            Ext.MessageBox.alert("Save", "Record saved.");
        }
    },

    onCancel: function(e) {
        var viewModel = this.getViewModel();
        var store = viewModel.getData().store;
        store.rejectChanges();
        e.up('window').close();
    },

    onDelete: function(e) {
        var viewModel = this.getViewModel();
        if (viewModel.getData().rec != null) {
            var store = viewModel.getData().store;
            Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete this record?', function (button) {
                if (button == 'yes') {
                    store.remove(viewModel.getData().rec);
                    store.sync();
                }
            });
        }
    },

    onAddDepartment: function(e) {
        var viewModel = this.getViewModel();
        viewModel.setData({ editMode: 'add' });
        var store = viewModel.getData().store;
        var record = Ext.create('WebHr.model.Department');
        var grid = this.lookupReference("departmentGrid");
        store.insert(0, record);
        grid.setSelection(record);
        this.getView().floatingItems.get('depWindow').show();
    }
});
