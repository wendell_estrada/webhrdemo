Ext.define('WebHr.view.department.DepartmentForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.departmentform',

    defaults: {
        xtype: 'textfield',
        anchor: '100%'
    },

    bodyPadding: 10,

    items: [
        {
            fieldLabel: 'Code',
            itemId: 'Code',
            bind: { value: '{rec.Code}' }
        },
        {
            fieldLabel: 'Name',
            itemId: 'Name',
            bind: { value: '{rec.Name}' }
        }
    ],

    buttons: [
        {
            text: 'Save',
            handler: 'onSave'
        },
        {
            text: 'Cancel',
            handler: 'onCancel'
        }
    ]
});
