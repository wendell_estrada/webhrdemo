/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('WebHr.Application', {
    extend: 'Ext.app.Application',

    name: 'WebHr',

    stores: [
        'WebHr.store.Employees',
        'WebHr.store.Departments',
        'WebHr.store.DepartmentEmployees'
    ],

    launch: function () {
        // TODO - Launch the application
    }
});
