﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebHR.Models
{
    public class HrDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }

        public HrDbContext()
            :base(@"Data Source=WENDELL\SQL2012;Initial Catalog=webhr;Integrated Security=SSPI;UID=sa;Pwd=masterkey")
        {
        }
    }
}