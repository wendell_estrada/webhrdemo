namespace WebHR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldTodepartment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Departments", "Code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Departments", "Code");
        }
    }
}
