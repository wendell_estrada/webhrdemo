﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebHR.Models;

namespace WebHR.Controllers
{
    public class DepartmentsController : ApiController
    {
        private HrDbContext db = new HrDbContext();

        // GET api/employees
        public IQueryable<Department> Get()
        {
            return db.Departments.OrderBy(e => e.Name);
        }

        // GET api/employees/5
        public Department Get(int id)
        {
            return db.Departments.First<Department>(e => e.Id == id);
        }

        // POST api/employees
        public async Task<HttpResponseMessage> Post(Department e)
        {
            if (!ModelState.IsValid)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            db.Departments.Add(e);
            await db.SaveChangesAsync();
            HttpResponseMessage rm = Request.CreateResponse(HttpStatusCode.OK, e);
            return rm;
        }

        // PUT api/employees/5
        public async Task<HttpResponseMessage> Put(int id, Department e)
        {
            if (!ModelState.IsValid)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            if (id != e.Id)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            db.Entry(e).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // DELETE api/employees/5
        public async Task<HttpResponseMessage> Delete(int id)
        {
            Department e = await db.Departments.FindAsync(id);
            if (e == null)
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            db.Departments.Remove(e);
            await db.SaveChangesAsync();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        private bool Exists(int id)
        {
            return db.Departments.Count(e => e.Id == id) > 0;
        }
    }
}