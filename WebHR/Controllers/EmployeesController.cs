﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHR.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace WebHR.Controllers
{
    public class EmployeesController : ApiController
    {

        private HrDbContext db = new HrDbContext();

        // GET api/employees
        public IQueryable<Employee> Get()
        {
            return db.Employees.OrderBy(e => e.FirstName);
        }

        // GET api/employees/5
        public Employee Get(int id)
        {
            return db.Employees.First<Employee>(e => e.Id == id);
        }

        // POST api/employees
        public async Task<HttpResponseMessage> Post(Employee e)
        {
            if (!ModelState.IsValid)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            db.Employees.Add(e);
            await db.SaveChangesAsync();
            HttpResponseMessage rm = Request.CreateResponse(HttpStatusCode.OK, e);
            return rm;
        }

        // PUT api/employees/5
        public async Task<HttpResponseMessage> Put(int id, Employee e)
        {
            if (!ModelState.IsValid)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            if (id != e.Id)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            db.Entry(e).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // DELETE api/employees/5
        public async Task<HttpResponseMessage> Delete(int id)
        {
            Employee e = await db.Employees.FindAsync(id);
            if (e == null)
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            db.Employees.Remove(e);
            await db.SaveChangesAsync();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.Id == id) > 0;
        }
    }
}
