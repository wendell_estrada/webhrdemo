﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHR.Models;

namespace WebHR.Controllers
{
    public class DepartmentEmployeesController : ApiController
    {
        private HrDbContext db = new HrDbContext();

        public IQueryable<Employee> Get(int id)
        {
            return db.Employees.Where(e => e.DepartmentId == id).OrderBy(e => e.LastName);
        }
    }
}
